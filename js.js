const $ = selector => document.querySelector(selector);
const $$ = selector => document.querySelectorAll(selector);

let rows = [];
const modal = $('.js_modal');
const modalAdd = $('.js_modal_add');
const tBody = $('.tbody');

// APIs
const apiGetUsers = () => {
    return fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json());
};

// DOM
const showRows = rows => {
    let htmlTableBody = '';
    rows.forEach(el => {
        htmlTableBody += createTemplate(el);
    });
    tBody.innerHTML = htmlTableBody;
    initEvents();

};

const createTemplate = el => {
    const {name, username, email, website, id} = el;
    return `<tr data-user-id="${id}">
                <td>${name}</td>
                <td>${username}</td>
                <td>${email}</td>
                <td>${website}</td>
                <td><div class="btn__close js_remove_user"></div></td>
            </tr>`
};

const initEvents = () => {
    $$('.tbody tr').forEach(item => item.addEventListener('click', showModal));
    $$('.js_remove_user').forEach(item => item.addEventListener('click', removeUser));
};

apiGetUsers()
    .then(commits => {
        showRows(commits);
        rows = commits;
    }).catch(err => console.log(err));

const showModal = ({target}) => {
    if (target.nodeName === 'TD') {
        const {phone, company} = getUserById(+target.parentNode.dataset.userId);
        $('.modal__phone').textContent = phone;
        $('.modal__company').textContent = company.name;

        modal.style.display = 'block';
    }
};

const getUserById = id => rows.find(el => el.id === id);

const closeModal = () => {
    modal.style.display = 'none';
    modalAdd.style.display = 'none';
};

$$('.js_modal_close').forEach(item => item.addEventListener('click', closeModal));

const removeUser = e => {
    e.stopPropagation();
    if (e.target.classList.contains('js_remove_user')) {
        const row = e.target.parentNode.parentNode;
        removeUserById(+row.dataset.userId);
        row.remove();
    }
};

const removeUserById = id => rows.forEach((el, i) => el.id === id ? rows.splice(i, 1) : '');

//Sort
const sortBy = param => () => {
    rows.sort((a, b) => {
        if (a[param] > b[param]) return 1;
        if (a[param] === b[param]) return 0;
        if (a[param] < b[param]) return -1;
    });
    showRows(rows);
};
$('.js_sort_by_name').addEventListener('click', sortBy('name'));
$('.js_sort_by_username').addEventListener('click', sortBy('username'));
$('.js_sort_by_email').addEventListener('click', sortBy('email'));
$('.js_sort_by_website').addEventListener('click', sortBy('website'));

$('.btn__add').addEventListener('click', () => modalAdd.style.display = 'block');

const getNewId = () => rows.reduce((maxId, el) => el.id > maxId ? el.id : maxId, 0) + 1;

const addUser = () => {
    const id = getNewId();
    const name = $('.modal_name').value === '' ? '-' : $('.modal_name').value;
    const username = $('.modal_username').value === '' ? '-' : $('.modal_username').value;
    const email = $('.modal_email').value === '' ? '-' : $('.modal_email').value;
    const website = $('.modal_website').value === '' ? '-' : $('.modal_website').value;
    const phone = $('.modal_phone').value === '' ? '-' : $('.modal_phone').value;
    const company = $('.modal_company').value === '' ? '-' : $('.modal_company').value;
    const newUser = {id, name, username, email, website, phone, company: {name: company}};
    rows.push(newUser);
    tBody.innerHTML = tBody.innerHTML + createTemplate(newUser);
    initEvents();
    clearModal();
};

$('.js_add').addEventListener('click', addUser);

const clearModal = () => {
    $$('.modal__description input').forEach(el => el.value = '');
    closeModal();
};

$('.js_modal_cancel').addEventListener('click', clearModal);
